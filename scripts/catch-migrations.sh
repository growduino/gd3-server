#!/bin/sh

MANAGE=~/gd3_server/manage.py

force_migrate () { 
    ${MANAGE} migrate core ${1} || ${MANAGE} migrate --fake core ${1}
}

normal_migrate () { 
    ${MANAGE} migrate core ${1}
}

if [ ! -e ~/first-squash ]; then 

    force_migrate 0002_auto_20171024_1700
    force_migrate 0003_auto_20171024_1726
    force_migrate 0004_rawdatastore
    force_migrate 0005_auto_20171121_1719
    force_migrate 0006_auto_20171121_1735
    force_migrate 0007_auto_20180320_1917
    force_migrate 0008_lcdline
    force_migrate 0009_auto_20180502_2037
    force_migrate 0010_auto_20180510_2059
    force_migrate 0011_auto_20180510_2213
    force_migrate 0012_auto_20180719_1736
    force_migrate 0013_auto_20180719_1738
    normal_migrate 0001_squashed_0013_auto_20180719_1738
    touch ~/first-squash
fi
