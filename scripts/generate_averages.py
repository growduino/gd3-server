from django.db import connection


def generate_averages():
    with connection.cursor() as cursor:
        cursor.execute(
            "with h as (select max(time) as time from core_sensordatahourly) delete from core_sensordatahourly where time = (select time from h)")
        cursor.execute(
            "with h as (select coalesce(max(time),'2018-01-01 00:00:00+00') as time from core_sensordatahourly) insert into core_sensordatahourly (value_f, sensor_id, time) select avg(value_f) value_f,sensor_id,date_trunc('hour', time) as time from core_sensordata where date_trunc('hour', time) > (select time from h) group by sensor_id,date_trunc('hour', time)")
        cursor.execute(
            "with h as (select max(time) as time from core_sensordatadaily) delete from core_sensordatadaily where time = (select time from h)")
        cursor.execute(
            "with h as (select coalesce(max(time),'2018-01-01 00:00:00+00') as time from core_sensordatadaily) insert into core_sensordatadaily (value_f, sensor_id, time) select avg(value_f) value_f,sensor_id,date_trunc('day', time) as time from core_sensordata where date_trunc('day', time) > (select time from h) group by sensor_id,date_trunc('day', time)")


def run():
    generate_averages()
