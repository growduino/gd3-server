from ..models import Alert, Trigger, TriggerLog, JsonStorage, OutputLog, SensorData, SensorDataDaily, SensorDataHourly, \
    new_config, save_config, CalibrationData
from ..views import announce_config_change


def clean_all():
    Alert.objects.all().delete()
    Trigger.objects.filter(index__gte=0).delete()
    TriggerLog.objects.all().delete()
    CalibrationData.objects.all().delete()

    # client = JsonStorage(filename='client', value='{usedAlerts":[0]}')
    # client.save()
    JsonStorage.objects.all().delete()

    OutputLog.objects.all().delete()
    SensorData.objects.all().delete()
    SensorDataHourly.objects.all().delete()
    SensorDataDaily.objects.all().delete()
    config = new_config('config')
    save_config(config, filename='config')
    announce_config_change('config')


def run():
    clean_all()
