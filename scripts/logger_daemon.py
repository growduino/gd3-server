# -*- coding: utf-8 -*-
import argparse
import datetime
import fcntl
import json
import logging
import os
import socket
import struct
import sys
import time

import daemon
import pika as pika
import requests
import serial


def get_ip_address(ifname=b'eth0'):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack(b'256s', ifname)
        )[20:24])
    except OSError:
        return None


# json decode error is value error in older pythons
try:
    json_decode_error = json.decoder.JSONDecodeError
except AttributeError:
    json_decode_error = ValueError


class Connection:
    def __init__(self, port, speed=115200, host='localhost', check_name=None, timeout=3):
        self.port = port
        self.speed = speed
        self.connection = serial.Serial(port, speed, timeout=timeout)
        self.name = ""  # arduino name, will be
        self.offset = 10  # Which second in minute we want to get data. Sensor reading starts at :00
        self.host = host  # hostname of django
        self.output_url = self.create_url('output_states.jso')  # url to get wanted output states from
        self.rabbit_response = self.create_url('rabbit.jso')  # url to respond to rabbitmq requests
        self.log_url = self.create_url('log')  # url to write sensor data to
        self.calib_url = self.create_url('calib.jso')  # url to get calibration data
        self.user = 'logger'  # user for http auth
        self.passwd = 'parodie na prvni dil'  # password for http auth
        self.age = -1

        try:
            self.read()  # get rid of Hello #name
        except UnicodeDecodeError:
            pass
        try:
            self.get_name(check_name)
        except UnicodeDecodeError:  # sets default name when current is garbage
            self.set_name("gd3")

        # rabbitmq connection
        self.rabbit_channel = None
        self.rabbit_queue_name = None
        self.get_rabbit_connection()

        self.data_line = None
        self.load_calibration()

    def close_rabbit_connection(self):
        self.rabbit_channel = None
        self.rabbit_queue_name = None

    def get_rabbit_connection(self):
        try:
            rabbit_connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
            self.rabbit_channel = rabbit_connection.channel()
            self.rabbit_channel.exchange_declare(exchange='sensors',
                                                 exchange_type='direct')
            result = self.rabbit_channel.queue_declare(exclusive=True)
            self.rabbit_queue_name = result.method.queue
            self.rabbit_channel.queue_bind(exchange='sensors',
                                           queue=self.rabbit_queue_name,
                                           routing_key=self.name)
        except pika.exceptions.ConnectionClosed:
            self.close_rabbit_connection()

    def create_url(self, text):
        """Format url for data upload
        :param text:path in url (after host name)
        :return text:full url
        """
        if self.host == 'localhost':
            url = "http://{host}:8080".format(host=self.host)
        else:
            url = "https://{host}".format(host=self.host)

        complete_url = '{url}/{text}'.format(url=url, text=text)
        return complete_url

    def read(self):
        """read line from serial port and return it as python string"""
        data = self.connection.readline()
        data = data.decode('utf-8', errors='ignore')
        logging.info("<<< " + data)
        return data

    def read_data(self, depth=0, reads=10):
        """get line from serial, parse and return dict, or None after too many failures/after getting ok
        Throws away debug messages"""
        if depth >= reads:
            self.data_line = None
            return None
        data = self.read()

        if data.lower().strip() == "ok":
            self.data_line = data
            return
        try:
            json_data = json.loads(data)
            self.data_line = data
        except json_decode_error:
            if data and data[0] == 'D':
                depth = depth
            else:
                depth = depth + 1
            time.sleep(depth / 5)
            json_data = self.read_data(depth=depth, reads=reads)
        return json_data

    def write_(self, data):
        """Convert data to bytestream and send to arduino as line"""
        if isinstance(data, bytes):
            data = data.decode('utf-8')
        logging.info(">>> " + data)
        data = data.strip() + "\n"
        data = data.encode('utf-8').replace(b'\xc2\xb0', b'\xdf')  # recode degree symbol to HD44780 charset
        self.connection.write(data)
        time.sleep(0.1)

    def robust_write(self, data, writes=3, reads=10):
        """write data to arduino and wait for response, repeating as needed"""
        self.data_line = None
        write_counter = 0
        json_data = None
        while self.data_line is None and write_counter < writes:
            write_counter += 1
            self.write_(data)
            json_data = self.read_data(reads=reads)

        if self.data_line is None:
            logging.debug("Robust read failed: {data}".format(data=data))
        else:
            logging.debug("Got data: {line}".format(line=self.data_line))
        return json_data

    def read_sensors(self):
        """get sensor data from arduino and convert"""
        data = self.robust_write("get sensors")
        return data

    def log(self, data, depth=0):
        """Log data to django"""
        if depth > 10:
            logging.error("grdw logger daemon: sending data to django failed")
            return None
        body = json.dumps(data)
        logging.debug("data inside logging")
        logging.debug(data)
        try:
            r = requests.post(self.log_url, auth=(self.user, self.passwd), data=body)
        except requests.exceptions.ConnectionError:
            logging.error("Exception in log request: {request}".format(request=body))
            return None
        logging.debug(''.join([
            "Resp: ", str(r.status_code),
            "\nContent: ", r.content.decode('utf-8'),
            "\nHeaders: ", str(r.headers)
        ]))
        try:
            result = json.loads(r.text)
        except json.decoder.JSONDecodeError:
            return self.log(data, depth + 1)
        if 'raw' in result:
            self.get_raw(result['raw'])

        try:
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            time.sleep(2)
            r = self.log(data, depth + 1)
        return r

    def switch(self, depth=0):
        """Get wanted output states from django and send them to arduino"""
        if depth > 10:
            logging.error("grdw logger daemon: switch() failed")
            return False
        try:
            r = requests.get(self.output_url)
            result = json.loads(r.text)
            logging.debug(result)
            self.set_outputs(result["outputs"])
            if 'raw' in result:
                self.get_raw(result['raw'])

        except ValueError as e:
            print(e)
            self.switch(depth + 1)

    def watching_sleep(self, seconds):
        start_time = datetime.datetime.now()
        duration = datetime.timedelta(seconds=seconds)
        stop_time = start_time + duration
        if self.rabbit_channel:
            try:
                consumer = self.rabbit_channel.consume(self.rabbit_queue_name, inactivity_timeout=1)
                logging.debug("Queue name {name}".format(name=self.rabbit_queue_name))
                for message in consumer:
                    logging.debug('msg {message}'.format(message=message))
                    if message and message[2] is not None:
                        msgtext = message[2].decode('utf-8')
                        data = "got " + msgtext
                        if msgtext == "calib":
                            self.load_calibration()
                        elif msgtext == "switch":
                            self.switch()
                        elif " " in msgtext:
                            data = self.robust_write(message[2])
                            self.log(data)
                        logging.debug(data)

                    if datetime.datetime.now() > stop_time:
                        return
            except pika.exceptions.ConnectionClosed:
                self.close_rabbit_connection()
                time_to_wait = stop_time - datetime.datetime.now()
                logging.debug(
                    "Sleeping for {sleep_time}s in watching sleep conn abort".format(sleep_time=time_to_wait.seconds)
                )

                if time_to_wait.seconds > 0:
                    time.sleep(time_to_wait.seconds)
                return
        else:
            logging.debug("Sleeping for {sleep_time}s in watching sleep".format(sleep_time=seconds))

            time.sleep(seconds)

    def poll(self):
        """Main worker procedure, contains daemon loop"""
        time.sleep(1)
        self.switch()
        while True:
            now = datetime.datetime.now()
            self.robust_write("set offset {offset}".format(offset=now.second))
            logging.debug(now)

            sleep_time = self.offset - now.second
            if sleep_time < 0:
                sleep_time = sleep_time + 60
            logging.debug("Sleeping for {sleep_time}s".format(sleep_time=sleep_time))
            if not self.rabbit_channel:
                self.get_rabbit_connection()
            self.watching_sleep(sleep_time)
            cntr = 0
            self.data_line = None
            data = None
            while self.data_line is None and cntr < 3:
                cntr += 1
                data = self.read_sensors()
            if data and self.data_line is not None:
                try:
                    self.age = data[0]['age']
                    self.log(data)
                except TypeError:
                    logging.error(
                        "grdw logger daemon: poll: Data read error: {type} {data}".format(type=type(data), data=data))
            else:
                logging.error("grdw logger daemon: poll: Data read error")
            self.watching_sleep(15)
            self.switch()

    def set_name(self, name):
        """Read board name from arduino"""
        logging.debug("Setting name")
        self.robust_write("set name " + name)

    def get_name(self, check_name=None):
        """Read board name from arduino"""
        logging.debug("Getting name")
        self.robust_write("get name")
        data = json.loads(self.data_line)
        self.name = data['name']
        logging.debug("Arduino name: {name}".format(name=self.name))
        if check_name is not None:
            if check_name != self.name:
                raise Exception("Name check failed")

    def set_outputs(self, param):
        """Send wanted output state to arduino"""
        self.robust_write("set outputs {outputs}".format(outputs=param))

    def get_raw(self, name):
        logging.debug("Getting raw value")
        self.robust_write("get raw {name}".format(name=name))
        data = json.loads(self.data_line)
        self.log(data)

    def load_calibration(self):
        try:
            r = requests.get(self.calib_url)
        except requests.exceptions.ConnectionError:
            logging.error("Cannot load calib data (Conn)")
            return
        try:
            calib_data = json.loads(r.text)
        except json.decoder.JSONDecodeError:
            logging.error("Cannot load calib data")
            calib_data = {}
        if 'calibration_data' in calib_data:
            logging.debug('Removing server-side calibration from calib_data')
            calib_data.pop('calibration_data')        
        for key in calib_data:
            val = calib_data[key]
            self.robust_write("set {name} {value}".format(name=key, value=val))


def report_lcd(connection):
    connection.robust_write("set lcd_clear")
    connection.robust_write("set lcd Connected")
    ipaddr = get_ip_address(b'eth0')
    if ipaddr:
        connection.robust_write("set lcd @{addr}".format(addr=ipaddr))
    ipaddr = get_ip_address(b'wlan0')
    if ipaddr:
        connection.robust_write("set lcd @{addr}".format(addr=ipaddr))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Growduino reader daemon')
    parser.add_argument('-p', '--port', help="usb port name")
    parser.add_argument('-d', '--debug', help='Debug (no detach)', action="store_true")
    parser.add_argument('-l', '--log', help="Loglevel (DEBUG, WARNING)")
    parser.add_argument('-H', '--host', help="Name of the host to connect to")
    parser.add_argument('-r', '--rename', help="Set new board name (and exit)")
    parser.add_argument('-n', '--name', help="Check name of board (fail if name differs)")

    print("Logger daemon starting at {now}".format(now=datetime.datetime.now().isoformat()))

    args = parser.parse_args()
    if not args.port:
        for port in ('USB0', 'ACM0', 'USB1', 'ACM1'):
            port_path = "/dev/tty{port}".format(port=port)
            if os.path.exists(port_path):
                args.port = port_path
                break

    if not args.port:
        raise ValueError('Usb port not found')

    if not args.host:
        args.host = "localhost"
    if not args.name or args.rename:
        args.name = None
    if not args.log:
        args.log = "warning"

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.log)

    if args.debug:
        logging.basicConfig(stream=sys.stdout, level=numeric_level)
        logging.debug("Starting")
        connection = Connection(args.port, host=args.host, check_name=args.name)
        time.sleep(3)
        if args.rename:
            connection.set_name(args.rename)
            sys.exit()
        time.sleep(1)
        report_lcd(connection)
        connection.poll()
    else:
        with daemon.DaemonContext():
            logging.basicConfig(filename='/tmp/logger_daemon.txt', level=numeric_level)
            logging.debug("Starting")
            connection = Connection(args.port, host=args.host, check_name=args.name)
            time.sleep(3)
            if args.rename:
                connection.set_name(args.rename)
                sys.exit()
            time.sleep(1)
            try:
                report_lcd(connection)

                connection.poll()
            except:
                logging.exception("exception in main loop")
