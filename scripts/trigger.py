# -*- coding: utf-8 -*-
from ..models import Trigger, OutputRec, LcdLine


def tick_triggers():
    for trigger in Trigger.objects.filter(active=1):
        trigger.tick()
        alerts = trigger.alert.all()
        for alert in alerts:
            alert.process_alert()

    for output in OutputRec.objects.all():
        output.tick()


def send_switch():
    import pika

    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange='sensors', exchange_type='direct')
    channel.basic_publish(exchange='sensors', routing_key='gd3', body="switch")


def send_lcd_lines():
    import pika

    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange='sensors', exchange_type='direct')
    channel.basic_publish(exchange='sensors', routing_key='gd3', body="set lcd_clear")

    for lcd_line in LcdLine.objects.all():
        channel.basic_publish(exchange='sensors', routing_key='gd3', body="set lcd " + lcd_line.display())


def run():
    tick_triggers()
    send_switch()
    send_lcd_lines()
