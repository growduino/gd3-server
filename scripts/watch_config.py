import json
import os
import sys
from datetime import datetime
from subprocess import call

"""
{"use_dhcp": false, "mac": "de:ad:be:ef:00:01", "ip": "192.168.1.101", "netmask": "255.255.255.0", "gateway": "192.168.1.21", "ntp": "195.113.56.8", "smtp": "10.38.253.137", "mail_from": "r-man@seznam.cz", "sys_name": "Growduino3", "smtp_port": "25", "time_zone": "1"}
"""

home = '/home/grdw/gd3_server/'

try:
    with open(home + "config.json") as f:
        new_config = json.load(f)

    with open(home + "config.json.bak") as f:
        old_config = json.load(f)
except FileNotFoundError:
    sys.exit(1)

try:
    if new_config['time_zone'] != old_config['time_zone']:
        call(['sudo', '/usr/bin/supervisorctl', 'restart', 'grdw:gunicorn'])
except KeyError:
    pass

if "enforce_ssl" not in new_config:
    new_config["enforce_ssl"] = False
call(['sudo', '/root/utility-scripts/set-ssl-redirect.sh', str(new_config['enforce_ssl'])])

time = str(int(datetime.now().timestamp()))
os.rename(os.path.join(home, 'config.json.bak'), os.path.join(home, 'config.json.bak.' + time))
