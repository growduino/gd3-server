import subprocess
import psutil
import os
load = str(os.getloadavg())

free_space = subprocess.check_output("df -h /".split(" ")).split(b'\n')[1].decode('utf-8')
free_space_root = [f for f in free_space.split(" ") if f != ''][4]

free_space = subprocess.check_output("df -h /var/log".split(" ")).split(b'\n')[1].decode('utf-8')
free_space_log = [f for f in free_space.split(" ") if f != ''][4]


temp = float(open('/sys/class/thermal/thermal_zone0/temp').read().strip())/1000
mem = psutil.virtual_memory()

#cpu = psutil.cpu_percent()

with open('/proc/uptime', 'r') as f:
    uptime = float(f.readline().split()[0])/3600

output = "Temperature {temp}C, uptime {uptime:.2f}, load {load}, used: disk space {free_space_root}, log {free_space_log}, mem {pct}%".format(
        free_space_root=free_space_root, 
        free_space_log=free_space_log,
        temp=temp,
        pct=mem.percent, 
        uptime=uptime,
        load=load,
        )

import logging
import logging.handlers

syslog = logging.getLogger('Minute checker')
syslog.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address='/dev/log')
syslog.addHandler(handler)
syslog.debug(output)

