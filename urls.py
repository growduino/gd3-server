"""growduino_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.views import defaults
from django.views.generic import RedirectView

from . import views

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/index.htm', permanent=False), name='home'),
    url(r'^alerts/(?P<index>[0-9]+).jso$', views.get_alert),
    url(r'^triggers/(?P<index>[0-9]+).jso$', views.get_trigger),
    url(r'^sensors/status.jso$', views.status),
    # /sensors/{tempX,humidity,light,...}.jso
    url(r'^sensors/outputs.jso$', views.outputs),
    url(r'^sensors/output_changes.jso$', views.output_changes),
    url(r'^output_states.jso$', views.output_states),
    url(r'^sensors/(?P<name>[0-9a-zA-Z]+).jso$', views.get_sensor_data),
    url(r'^sensors/(?P<name>[0-9a-zA-Z]+).html$', views.get_sensor_data, {'html': True}),

    url(r'^sensors/rawdata/(?P<index>[0-9]+).jso$', views.calibrate),

    url(
        r'^(data|DATA)/(outputs|OUTPUTS)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+)/(?P<sequence>[0-9]+).(jso|JSO)$',
        views.outputs),
    url(
        r'^(data|DATA)/(output_changes|OUTPUT_CHANGES)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+)/(?P<sequence>[0-9]+).jso$',
        views.output_changes),

    url(
        r'^(data|DATA)/(?P<name>[0-9a-zA-Z]+)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+)/(?P<hour>[0-9]+).(jso|JSO)$',
        views.get_hourly_data),
    url(
        r'^(data|DATA)/(?P<name>[0-9a-zA-Z]+)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+)/(?P<hour>[0-9]+).html$',
        views.get_hourly_data, {'html': True}),

    url(r'^logger/(?P<name>[0-9a-zA-Z]+)/(?P<value>[0-9.]*)$', views.sensor_log),
    url(r'^log$', views.sensor_log),

    url(r'^(?P<filename>config).jso$', views.json_store),
    url(r'^(?P<filename>client).jso$', views.json_store),
    url(r'^(?P<filename>calib).jso$', views.json_store),
    url(r'^wifilist.jso$', views.wifi_list),
    url(r'^wifi_active.jso$', views.wifi_active),

    url(r'^partial/(?P<filename>config).jso$', views.json_store_partial),
    url(r'^partial/(?P<filename>client).jso$', views.json_store_partial),
    url(r'^partial/(?P<filename>calib).jso$', views.json_store_partial),

    url(r'^(data|DATA)/(?P<name>[0-9a-zA-Z]+)/(?P<year>[0-9]+)/(?P<month>[0-9]+).(jso|JSO)$',
        views.get_daily_average),

    url(r'^(data|DATA)/(?P<name>[0-9a-zA-Z]+)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+).(jso|JSO)$',
        views.get_hourly_average),

    url(r'^(data|DATA)/(?P<name>[0-9a-zA-Z]+)/(?P<year>[0-9]+)/(?P<month>[0-9]+).html$', views.get_daily_average,
        {'html': True}),

    url(r'^(data|DATA)/(?P<name>[0-9a-zA-Z]+)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+).html$',
        views.get_hourly_average, {'html': True}),
    url(r'^change_password', views.password_change),
    url('^webcam', defaults.page_not_found),
    url('^send_test_mail', views.send_test_mail),

    url(r'^calibrate/(?P<sensor>[0-9a-zA-Z]+)/new$', views.calib_new),
    url(r'^calibrate/(?P<sensor>[0-9a-zA-Z]+)/list.jso$', views.calib_list),
    url(r'^calibrate/(?P<sensor>[0-9a-zA-Z]+)/get/(?P<id>[0-9]+).jso$', views.calib_get),
    url(r'^calibrate/(?P<sensor>[0-9a-zA-Z]+)/set/(?P<id>[0-9]+).jso$', views.calib_set),
    url(r'^calibrate/(?P<sensor>[0-9a-zA-Z]+)/delete/(?P<id>[0-9]+).jso$', views.calib_delete),

]
