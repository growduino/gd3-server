# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import TargetBoard, JsonStorage, StateLoggedModel, OutputRec, CalibrationData, Sensor, SensorData, SensorDataHourly, SensorDataDaily, Trigger, Alert, TriggerLog, OutputLog, AlertLog, EventTimes, RawDataStore, LcdLine


@admin.register(TargetBoard)
class TargetBoardAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'sensor_count', 'output_count')
    search_fields = ('name',)


@admin.register(JsonStorage)
class JsonStorageAdmin(admin.ModelAdmin):
    list_display = ('id', 'value', 'filename', 'time')
    list_filter = ('time',)


@admin.register(StateLoggedModel)
class StateLoggedModelAdmin(admin.ModelAdmin):
    list_display = ('id',)


@admin.register(OutputRec)
class OutputRecAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'index',
        'target_index',
        'target_name',
        'disabled',
    )
    list_filter = ('target_name', 'disabled')
    search_fields = ('name',)


@admin.register(CalibrationData)
class CalibrationDataAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'sensor_reading',
        'real_value',
        'timestamp',
        'sensor',
    )
    list_filter = ('timestamp', 'sensor')


@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'index',
        'target_index',
        'target_name',
        'divisor',
        'visual_divisor',
        'offset',
        'slope',
        'power',
    )
    list_filter = ('target_name',)
    search_fields = ('name',)


@admin.register(SensorData)
class SensorDataAdmin(admin.ModelAdmin):
    list_display = ('id', 'time', 'value_f', 'sensor')
    list_filter = ('time',)
    raw_id_fields = ('sensor',)


@admin.register(SensorDataHourly)
class SensorDataHourlyAdmin(admin.ModelAdmin):
    list_display = ('id', 'time', 'value_f', 'sensor')
    list_filter = ('time',)
    raw_id_fields = ('sensor',)


@admin.register(SensorDataDaily)
class SensorDataDailyAdmin(admin.ModelAdmin):
    list_display = ('id', 'time', 'value_f', 'sensor')
    list_filter = ('time',)
    raw_id_fields = ('sensor',)


@admin.register(Trigger)
class TriggerAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'index',
        't_since',
        't_until',
        'on_value',
        'off_value',
        'sensor',
        'output',
        'active',
    )


@admin.register(Alert)
class AlertAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'index',
        'on_message',
        'off_message',
        'trigger',
        'target',
        'active',
    )
    list_filter = ('trigger',)


@admin.register(TriggerLog)
class TriggerLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'trigger', 'state', 'time')
    list_filter = ('state', 'time')
    raw_id_fields = ('trigger',)


@admin.register(OutputLog)
class OutputLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'output', 'state', 'time')
    list_filter = ('state', 'time')
    raw_id_fields = ('output',)


@admin.register(AlertLog)
class AlertLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'alert', 'state', 'time')
    list_filter = ('alert', 'state', 'time')


@admin.register(EventTimes)
class EventTimesAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'time')
    list_filter = ('time',)


@admin.register(RawDataStore)
class RawDataStoreAdmin(admin.ModelAdmin):
    list_display = ('id', 'sensor', 'value', 'measured')
    list_filter = ('sensor', 'measured')


@admin.register(LcdLine)
class LcdLineAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'priority',
        'text',
        'error_text',
        'error_age',
        'sensor',
        'is_datetime',
    )
    list_filter = ('sensor', 'is_datetime')
