from django.test import TestCase

# Create your tests here.


from .models import Sensor, OutputRec


class SensorTestCase(TestCase):
    def setUp(self):
        Sensor.objects.create(name='test1', index=1)
        Sensor.objects.create(name='test2', index=2)

    def test_sensor_create_and_log(self):
        test1 = Sensor.objects.get(index=1)
        test2 = Sensor.objects.get(index=2)

        test1.log(10)
        test2.log(20)
        self.assertEqual(test1.data.latest().value, 10)
        self.assertEqual(test2.data.latest().value, 20)

    def test_calib_offset(self):
        test1 = Sensor.objects.get(index=1)
        test2 = Sensor.objects.get(index=2)

        test1.offset = 10
        test2.offset = 20

        test1.save()
        test2.save()

        self.assertEqual(test1.log(10), 20)
        self.assertEqual(test2.log(20), 40)

    def test_calib_slope(self):
        test1 = Sensor.objects.get(index=1)
        test2 = Sensor.objects.get(index=2)

        test1.slope = 2
        test2.slope = 4

        test1.save()
        test2.save()

        self.assertEqual(test1.log(10), 20)
        self.assertEqual(test2.log(20), 80)

    def test_calib_shift(self):
        test1 = Sensor.objects.get(index=1)
        test2 = Sensor.objects.get(index=2)

        test1.inverse_shift = 0
        test2.inverse_shift = 0.1

        test1.save()
        test2.save()

        self.assertEqual(test1.log(10), 10)
        self.assertEqual(test2.log(10), 5)

    def test_log_latest(self):
        test1 = Sensor.objects.get(index=1)
        self.assertEqual(test1.log(10), 10)

        self.assertEqual(test1.log(11), 11)
        self.assertEqual(test1.log(12), 12)
        self.assertEqual(test1.log(13), 13)

        self.assertEqual(test1.data.latest().value, 13)


class OutputTestCase(TestCase):
    def setUp(self):
        OutputRec.objects.create(index=0, name="test0")

    def test_created(self):
        output = OutputRec.objects.get(index=0)
        self.assertEqual(output.name, "test0")
        self.assertFalse(output.disabled)
        self.assertFalse(output.state)

    def test_toggle(self):
        output = OutputRec.objects.get(index=0)
        self.assertTrue(output.switch(True))
        self.assertTrue(output.state)
