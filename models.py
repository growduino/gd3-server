# -*- coding: utf-8 -*-
import datetime
import json
import os
from os.path import isfile
from shutil import copyfile

import math
from django.conf import settings
from django.core import mail
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail.backends.smtp import EmailBackend
from django.db import models
from django.utils import timezone
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from math import isnan, isinf

from .config import MINVALUE  # , get_config
from .utils import crop_seconds, get_daymin, add_tzinfo, get_mac

# Create your models here.

STATES = (
    (0, False),
    (1, True),
    (2, 'Always on'),
)


def get_config(field=None, filename='config'):
    try:
        config_db = JsonStorage.objects.filter(filename=filename).latest()
        config = json.loads(config_db.value)
    except ObjectDoesNotExist:
        config = new_config(filename)

    if filename == 'calib':
        calibration_data = {l.id: l.as_dict() for l in CalibrationData.objects.all()}
        config['calibration_data'] = calibration_data
    if filename == 'config':
        config['mac'] = get_mac('eth0')
    if field is None:
        return config
    else:
        if field in config:
            return config[field]
        else:
            return None


def new_config(filename):
    if filename == 'config':
        config = json.loads("""{
    "use_dhcp": 1,
    "mac": "",
    "ip": "192.168.1.101",
    "netmask": "255.255.255.0",
    "gateway": "192.168.1.1",
    "ntp": "195.113.56.8",
    "smtp": "10.38.253.137",
    "mail_from": "nobody@growduino.cz",
    "sys_name": "Growduino3",
    "smtp_port": "25",
    "time_zone": "1"
}""")
        config['mac'] = get_mac('eth0')
    else:
        config = dict()
    return config


def save_config_to_file(config, filename):
    config_dir = getattr(settings, 'BASE_DIR', None)
    filename = os.path.join(config_dir, ".".join([filename, 'json']))
    # print(filename)
    if isfile(filename):
        dst = filename + ".bak"
        if not isfile(dst):
            copyfile(filename, dst)
    with open(filename, "w") as f:
        if not isinstance(config, str):
            config = json.dumps(config)
        f.write(config)


def save_config(config, filename='config'):
    """ saves dict config into database. """

    if filename == 'calib' and 'calibration_data' in config:
        calibration_data = config.pop('calibration_data')
        print(config)
        print("***")
        print(type(calibration_data))
        print(calibration_data)
        indexes = []
        for id in calibration_data:
            indexes.append(id)
            from_file = calibration_data[id]
            print("---")
            print(type(from_file))
            print(from_file)
            calib_data, created = CalibrationData.objects.get_or_create(id=id,
                                                                        defaults={'sensor_reading': 0, 'real_value': 0,
                                                                                  'sensor': Sensor.objects.get(
                                                                                      index=from_file['sensor'])})
            calib_data.sensor_reading = from_file['sensor_reading']
            calib_data.real_value = from_file['real_value']
            calib_data.sensor = Sensor.objects.get(index=from_file['sensor'])
            calib_data.save()
        CalibrationData.objects.exclude(id__in=indexes).delete()

    for sensor in Sensor.objects.all():
        sensor.update_calibration()

    config = json.dumps(config)

    config_db = JsonStorage(value=config, filename=filename)
    config_db.save()
    if filename == 'config':
        save_config_to_file(config, filename)


def update_config(key, val):
    config = get_config()
    if config[key] != val:
        config[key] = val
        save_config(config)
        return True
    return False


class TargetBoard(models.Model):
    name = models.CharField(verbose_name=_('board name'), default="gd3", max_length=32)
    sensor_count = models.IntegerField(verbose_name=_('number of sensors on board'), default=10)
    output_count = models.IntegerField(verbose_name=_('number of outputs on board'), default=10)


class JsonStorage(models.Model):
    class Meta:
        verbose_name = _("configuration")
        verbose_name_plural = _("configurations")
        ordering = ["time"]
        get_latest_by = 'time'

    def save(self, *args, **kwargs):
        """ On save, update timestamp """
        self.time = timezone.now()
        return super(JsonStorage, self).save(*args, **kwargs)

    value = models.CharField(verbose_name=_('serialized config'), max_length=2048)
    filename = models.CharField(verbose_name=_('file name'), max_length=2048)
    time = models.DateTimeField()


class StateLoggedModel(models.Model):
    @property
    def state(self):
        """Returns state according to database"""
        return self.get_state()

    @property
    def uptime(self):
        """Returns time since last change with 60s granularity, false when there are no changes in database"""
        try:
            log = self.log.latest()
            return crop_seconds() - log.time
        except ObjectDoesNotExist:
            return False

    def get_state(self, time=None):
        try:
            if time is None:
                log = self.log.latest()
            else:
                log = self.log.filter(time__lte=time).latest()
            return log.state
        except ObjectDoesNotExist:
            return False


class OutputRec(StateLoggedModel):
    name = models.CharField(verbose_name=_('output name'), max_length=32)
    index = models.IntegerField(unique=True, verbose_name=_('index of output in interface'))
    target_index = models.IntegerField(verbose_name=_('index of output on target, if different from index'), blank=True,
                                       null=True)
    target_name = models.ForeignKey(TargetBoard, related_name='output', default=1)
    disabled = models.BooleanField(default=False)

    # Functions
    def __str__(self):
        return _("output #%s - %s") % (self.index, self.name)

    def switch(self, new_state):
        """Switches output on/off and logs the change to database"""
        if new_state != self.state or self.uptime is False:
            log = OutputLog(output=self, state=new_state)
            log.save()
            return new_state
        return self.state

    def tick(self, forced=False):
        disable = False
        new_state = False

        if (self.trigger.filter(active=2)):
            return self.switch(True)
        for trigger in self.trigger.filter(active=1):
            if not trigger.important:  # triggers with ! do not directly affect output state
                new_state = new_state or trigger.state

            if trigger.important and trigger.on_time() and not trigger.state:
                disable = True

        if disable:
            self.disabled = True
            ret = self.switch(False)
            self.save()
            return ret
        else:
            self.disabled = False
            self.save()

        if not disable:
            if self.state is False and new_state is True:
                return self.switch(True)
            elif self.state is True and new_state is False:
                return self.switch(False)

    class Meta:
        verbose_name = _('output')
        verbose_name_plural = _("outputs")
        ordering = ["index"]


class CalibrationData(models.Model):
    class Meta:
        ordering = ['id']

    sensor_reading = models.FloatField(_("value read from sensor"))
    real_value = models.FloatField(_("User measured value"))
    timestamp = models.DateTimeField(_("time of measurement"), auto_now=True)

    sensor = models.ForeignKey("Sensor", related_name='CalibrationData', on_delete=models.CASCADE)

    def as_dict(self):
        ret = {'id': self.id,
               'sensor_reading': self.sensor_reading,
               'real_value': self.real_value,
               'timestamp': self.timestamp,
               'sensor': self.sensor.index
               }
        return ret


class Sensor(models.Model):
    name = models.CharField(verbose_name=_('sensor name'), max_length=32)
    index = models.IntegerField(unique=True, verbose_name=_('index of sensor in interface'))
    target_index = models.IntegerField(verbose_name=_('index of output on target, if different from index'), blank=True,
                                       null=True)
    target_name = models.ForeignKey(TargetBoard, related_name='sensor', default=1)
    divisor = models.FloatField(default=0)
    visual_divisor = models.FloatField(_("divisor for non-json views"), default=1)
    offset = models.FloatField(_('zero degree correction'), default=0)
    slope = models.FloatField(_('first degree correction'), default=1)
    power = models.FloatField(_('2nd degree correction'), default=1)

    def update_calibration(self):
        calibration_data = self.CalibrationData.all().order_by('-timestamp')
        calibration_data_count = calibration_data.count()
        if calibration_data_count == 0:
            self.offset = 0
            self.slope = 1
            self.power = 1
        elif calibration_data_count == 1:
            point_1 = calibration_data[0]
            self.offset = point_1.real_value - point_1.sensor_reading
            self.slope = 1
            self.power = 1
        elif calibration_data_count >= 2:  # in case of more records, use only 2 newest (explicit ordering)
            point_1 = calibration_data[0]
            point_2 = calibration_data[1]
            # (real2-real1)/(sensor2-sensor1)
            self.slope = (point_2.real_value - point_1.real_value) / (point_2.sensor_reading - point_1.sensor_reading)
            # real - sensor * slope
            self.offset = point_1.real_value - point_1.sensor_reading * self.slope
            self.power = 1
        else:
            pass
            # import numpy
            # import scipy.optimize
            #
            # xdata = []
            # ydata = []
            # for point in calibration_data:
            #     xdata.append(point.real_value)
            #     ydata.append(point.sensor_reading)
            #     x0 = numpy.array([0.0, 0.0, 0.0])
            #     sigma = numpy.array([1.0] * len(xdata))
            #
            #     def func(x, a, b, c):
            #         return a + b * x + c * x * x
            #
            #     offset, slope, power = scipy.optimize.curve_fit(func, xdata, ydata, x0, sigma)[0]
            #     self.offset = offset
            #     self.slope = slope
            #     self.power = power

        return self.save()

    # Functions
    def log(self, value, offset=None):
        try:
            value = float(value)
        except ValueError:
            return None
        when = crop_seconds()

        if offset is not None:
            when = when - datetime.timedelta(0, 60 * offset, 0)

        if not math.isfinite(value):
            return None

        value = self.adjust(value)

        log_record, created = SensorData.objects.get_or_create(sensor=self, time=when)
        log_record.set(value)
        log_record.save()
        return value

    def __str__(self):
        return _("sensor #%s - %s") % (self.index, self.name)

    def adjust(self, value):
        if self.power == 0 and self.slope != 0:
            return value * self.slope + self.offset
        else:
            return self.slope * value ** self.power + self.offset


class SensorDataTemplate(models.Model):
    # Attributes
    time = models.DateTimeField(verbose_name=_('time of logging'))
    value_f = models.FloatField(verbose_name=_('logged value'), null=True, blank=True)

    class Meta:
        abstract = True

    # Functions
    def __repr__(self):
        return "record from {name} at {time} with value of {value}".format(name=self.sensor.name, time=self.time,
                                                                           value=self.value)

    @property
    def value(self):
        if self.sensor.divisor:
            try:
                return int(self.value_f / self.sensor.divisor)
            except (TypeError, ValueError):
                return MINVALUE
        return self.value_f

    def set_(self):
        pass

    def set(self, value=None):
        if value == MINVALUE or isinf(value) or isnan(value):
            value = None
        self.value_f = value
        if value is not None:  # only valid data should be saved to database
            self.save()

    @property
    def age(self):
        return (timezone.now() - self.time).seconds


class SensorData(SensorDataTemplate):
    # Relations
    sensor = models.ForeignKey(Sensor,
                               related_name='data',
                               verbose_name=_('sensor'),
                               )

    # Meta
    class Meta:
        verbose_name = _("sensor record")
        verbose_name_plural = _("sensor records")
        ordering = ["time", "sensor"]
        get_latest_by = 'time'
        index_together = ["time", "sensor"]

    def set_(self):
        last_update = EventTimes.objects.get_or_create(event="CountAverages",
                                                       defaults={'time': datetime.date(2017, 1, 1)},
                                                       )
        if now() - last_update > datetime.timedelta(hours=1):
            pass


class SensorDataHourly(SensorDataTemplate):
    sensor = models.ForeignKey(Sensor,
                               related_name='hourly_average',
                               verbose_name=_('sensor'),
                               )

    class Meta:
        verbose_name = _("sensor hourly average")
        verbose_name_plural = _("sensor hourly averages")
        ordering = ["time", "sensor"]
        get_latest_by = 'time'


class SensorDataDaily(SensorDataTemplate):
    sensor = models.ForeignKey(Sensor,
                               related_name='daily_average',
                               verbose_name=_('sensor'),
                               )

    class Meta:
        verbose_name = _("sensor hourly average")
        verbose_name_plural = _("sensor hourly averages")
        ordering = ["time", "sensor"]
        get_latest_by = 'time'


class Trigger(StateLoggedModel):
    #    """    {
    #        "t_since":-1,
    #        "t_until":0,
    #        "on_value":"<250",
    #        "off_value":">300",
    #        "sensor":1,
    #        "output":7,
    #        "active":1
    #    }
    #    """

    @property
    def important(self):
        """Importance is noted by "!" at end of off condition,
        and means that this condition is critical,
        if off condition is true it disables that output until on condition is met"""
        try:
            return self.off_value[-1] == '!'
        except IndexError:
            return False

    @property
    def sensor_val(self):
        """Reads value from sensor checked by this trigger"""
        if self.sensor.index == -1:
            return MINVALUE
        try:
            sensor_val = self.sensor.data.latest()
        except ObjectDoesNotExist:
            sensor_val = None

        if sensor_val is None or sensor_val.value is None or sensor_val.age > 120:
            # We do not have good, recent value
            return False
        return sensor_val.value

    def switch(self, new_state, forced=False):
        """change state to new_state and log to database"""
        # print("state", self.state, "new", new_state)
        if (new_state != self.state) or forced:
            # print("logging")
            log, created = TriggerLog.objects.get_or_create(trigger=self, time=crop_seconds(),
                                                            defaults={'state': new_state})
            log.state = new_state
            # print("created:", created)
            log.save()
            # print(log)
            # return self.output.switch(new_state)
            return True
        return False

    def on_time(self):
        """checks if the trigger should be ticking at this time of day"""
        daymin = get_daymin()

        if self.t_since == -1:
            return True

        if self.t_until < self.t_since:  # over midnight
            return (self.t_until > daymin) or (self.t_since < daymin)
        else:
            return self.t_since <= daymin < self.t_until

    @property
    def on_cmp(self):
        return self.on_value[0].lower()

    @property
    def on_val(self):
        return float(self.on_value[1:])

    @property
    def off_val(self):
        off_val = self.off_value[1:]
        if self.important:
            off_val = off_val[:-1]
        off_val = float(off_val)
        return off_val

    @property
    def off_cmp(self):
        return self.off_value[0].lower()

    def tick(self, forced=False):
        """Checks trigger conditions, sets state. Returns true on changing state
        on forced saves state even if not changing"""
        # print("ticking")

        if self.active == 0:  # never
            return self.switch(False, forced)

        if self.active == 2:  # always
            return self.switch(True, forced)

        sensor_value = self.sensor_val
        time = self.on_time()
        wanted_state = False  # True means we want to switch output on

        # if self.state:
        #     print("sensor_val", sensor_value, "state", self.state, "off_cmp:", self.off_cmp, "off_val", self.off_val)
        # else:
        #     print("sensor_value", sensor_value, "state", self.state, "on_cmp:", self.on_cmp, "on_val", self.on_val)
        #
        # print("Time", time, "on", self.t_since, "off", self.t_until, "daymin", get_daymin())

        if time:
            # time is ok
            # print("Time ok")
            # print("State", self.state)
            if not self.state:  # we are switched off
                # print("trying to turn on")

                if self.on_cmp == 't':
                    # try:
                    #     print("t", self.on_val, self.output.uptime.seconds)
                    # except AttributeError:
                    #     print("t", self.on_val, " no output uptime")
                    if self.output:
                        wanted_state = (
                                (self.output.state is False) and (self.output.uptime is False or
                                                                  self.output.uptime.seconds >= (self.on_val * 60)))

                elif self.on_cmp == '<' and sensor_value is not False:
                    wanted_state = sensor_value < self.on_val
                elif self.on_cmp == '>' and sensor_value is not False:
                    wanted_state = sensor_value > self.on_val
                    # print("on target", wanted_state)

            else:  # trigger was on.
                # print("trying to turn off")

                if self.off_cmp == 't':
                    # print("t", self.off_val, self.output.uptime.seconds)
                    if self.output and self.output.state:
                        wanted_state = self.output.uptime.seconds < (self.off_val * 60)
                elif self.off_cmp == '<':
                    wanted_state = sensor_value >= self.off_val
                elif self.off_cmp == '>':
                    wanted_state = sensor_value <= self.off_val
                # print(wanted_state)

        # print("result is", wanted_state)
        return self.switch(wanted_state, forced)

    def as_dict(self):
        toret = dict()
        toret['t_since'] = self.t_since
        toret['t_until'] = self.t_until
        toret['on_value'] = self.on_value
        toret['off_value'] = self.off_value
        toret['sensor'] = self.sensor.index
        toret['output'] = self.output.index
        toret['active'] = self.active
        return toret

    index = models.IntegerField(unique=True, verbose_name=_('trigger ID'))
    t_since = models.IntegerField(verbose_name=_('trigger start time'), default=-1)
    t_until = models.IntegerField(verbose_name=_('trigger end time'), default=-1)
    on_value = models.CharField(verbose_name=_('on condition and value'), max_length=20)
    off_value = models.CharField(verbose_name=_('off condition and value'), max_length=20)
    sensor = models.ForeignKey(Sensor,
                               null=True,
                               related_name='trigger',
                               verbose_name=_('sensor'),
                               )
    output = models.ForeignKey(OutputRec,
                               null=True,
                               related_name='trigger',
                               verbose_name=_('controlled output'),
                               )
    active = models.IntegerField(verbose_name=_('is trigger active?'), default=0)

    def __str__(self):
        if self.active == 1:
            act = "active"
        elif self.active == 2:
            act = "always on"
        else:
            act = "disabled"
        return _("%s trigger %s sensor %s output %s") % (act, self.index, self.sensor, self.output)

    class Meta:
        verbose_name = _("trigger rule")
        verbose_name_plural = _("trigger rules")
        ordering = ["index"]


class Alert(models.Model):
    #        """
    #  {
    #      "on_message":"Temp too high!",
    #      "off_message":"Temp back at normal",
    #      "trigger":1,
    #      "target":"+420777123456",
    #  }
    # """

    index = models.IntegerField(verbose_name=_('alert ID'), unique=True)
    on_message = models.CharField(verbose_name=_('message to send on alarm start'), max_length=1000)
    off_message = models.CharField(verbose_name=_('message to send on alarm end'), max_length=1000)
    trigger = models.ForeignKey(Trigger,
                                related_name='alert',
                                verbose_name=_('alert'),
                                on_delete=models.CASCADE,
                                )
    target = models.CharField(verbose_name=_('mesage destination'), max_length=200)
    active = models.IntegerField(verbose_name=_('is alert active?'), default=0)

    def __str__(self):
        return _("Alert on trigger %s") % (self.trigger.index,)

    def process_alert(self):
        """ sends alert if its trigger has changed"""
        if not self.active:
            return False
        try:
            last_log = AlertLog.objects.filter(alert=self).latest()

        except ObjectDoesNotExist:
            last_log = None

        if (last_log is None and self.trigger.state is True) or (
                last_log is not None and last_log.state != self.trigger.state):
            new_log = AlertLog(
                # trigger=self.trigger,
                alert=self,
                state=self.trigger.state
            )

            if self.trigger.state:
                subject = "[{sys_name}] alert".format(sys_name=get_config('sys_name'))
                body = self.on_message
            else:
                body = self.off_message
                subject = "[{sys_name}] end of alert".format(sys_name=get_config('sys_name'))

            val = self.trigger.sensor.data.latest().value * self.trigger.sensor.visual_divisor
            body = body + "\nValue: {val:.2f}".format(val=val)

            if "," in self.target:
                target = self.target.split(",")
            else:
                target = [self.target]

            new_log.save()

            host = get_config('smtp')
            port = get_config('smtp_port')
            username = get_config('smtp_user')
            password = get_config('smtp_pwd')
            use_tls = get_config('smtp_ssl') == 'tls'
            use_ssl = get_config('smtp_ssl') == 'ssl'

            backend = EmailBackend(
                host=host,
                port=port,
                username=username,
                password=password,
                use_ssl=use_ssl,
                use_tls=use_tls,
                timeout=30,
            )
            mail.EmailMessage(
                subject=subject,
                body=body,
                from_email=get_config('mail_from'),
                to=target,
                connection=backend,
            ).send()

    class Meta:
        verbose_name = _("alert rule")
        verbose_name_plural = _("alert rules")
        ordering = ["index"]


class TriggerLog(models.Model):
    def save(self, *args, **kwargs):
        """ On save, update timestamp """
        self.time = crop_seconds()
        return super(TriggerLog, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("trigger change log")
        verbose_name_plural = _("trigger change logs")
        ordering = ["-time"]
        get_latest_by = 'time'

    def __str__(self):
        if self.state:
            act = "revive"
        else:
            act = "disable"
        return _("%s from trg %s at %s") % (act, self.trigger.index, self.time)

    trigger = models.ForeignKey(Trigger,
                                related_name='log',
                                verbose_name=_('log'),
                                )
    state = models.BooleanField()
    time = models.DateTimeField()


class OutputLog(models.Model):
    def save(self, *args, **kwargs):
        """ On save, update timestamp """
        self.time = crop_seconds()
        return super(OutputLog, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("output change log")
        verbose_name_plural = _("output change logs")
        ordering = ["time"]
        get_latest_by = 'time'

    output = models.ForeignKey(OutputRec,
                               related_name='log',
                               verbose_name=_('log'),
                               )
    state = models.BooleanField()
    time = models.DateTimeField()


class AlertLog(models.Model):
    def save(self, *args, **kwargs):
        """ On save, update timestamp """
        self.time = crop_seconds()
        return super(AlertLog, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("alert log")
        verbose_name_plural = _("alert logs")
        ordering = ["time"]
        get_latest_by = 'time'

    alert = models.ForeignKey(Alert,
                              related_name='log',
                              verbose_name=_('log'),
                              )
    state = models.BooleanField()
    time = models.DateTimeField()


class EventTimes(models.Model):
    event = models.CharField(max_length=100)
    time = models.DateField()


class RawDataStore(models.Model):
    sensor = models.ForeignKey(Sensor,
                               related_name='raw_data',
                               verbose_name=_('raw data'),
                               )
    value = models.FloatField(_("last read raw value"), blank=True, null=True)
    measured = models.BooleanField(default=False)


class LcdLine(models.Model):
    class Meta:
        ordering = ['priority']
        verbose_name = _("line to display on lcd")
        verbose_name_plural = _("line to display on lcd")

    def display(self):
        out = self.text
        if self.is_datetime:
            now = add_tzinfo(datetime.datetime.now())
            out = now.strftime("%Y-%m-%d %H:%M")
        else:
            try:
                if "{val" in self.text:
                    reading = self.sensor.data.latest()
                    if not reading or (self.error_age > 0 and reading.age > (self.error_age * 60)):
                        out = self.error_text
                    else:
                        val = self.sensor.data.latest().value
                        if self.sensor:
                            val = val * self.sensor.visual_divisor
                        out = self.text.format(val=val)
            except KeyError:
                pass
            except ObjectDoesNotExist:
                return self.error_text
        return out

    priority = models.IntegerField(_("order lower first"), unique=True)
    text = models.CharField(_("text to display, use {val} to read sensor"), max_length=100, blank=True)
    error_text = models.CharField(_("Text to display if last sensor read is older than age minutes"), max_length=100,
                                  blank=True)
    error_age = models.IntegerField(_("Age of last reading to be declared faulty, 0 to disable"), default=0)
    sensor = models.ForeignKey(Sensor,
                               related_name='lcd_lines',
                               blank=True,
                               null=True
                               )
    is_datetime = models.BooleanField(_("check to display clock and not text"), default=False)
