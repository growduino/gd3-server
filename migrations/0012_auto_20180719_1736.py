# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-07-19 16:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_auto_20180510_2213'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lcdline',
            name='int_shift',
        ),
        migrations.AddField(
            model_name='sensor',
            name='visual_divisor',
            field=models.FloatField(default=0),
        ),
    ]
