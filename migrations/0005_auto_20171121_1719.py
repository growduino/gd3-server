# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-21 17:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_rawdatastore'),
    ]

    operations = [
        migrations.CreateModel(
            name='TargetBoard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='gd3', max_length=32, verbose_name='board name')),
                ('sensor_count', models.IntegerField(default=10, verbose_name='number of sensors on board')),
                ('output_count', models.IntegerField(default=10, verbose_name='number of outputs on board')),
            ],
        ),
        migrations.AddField(
            model_name='outputrec',
            name='target_index',
            field=models.IntegerField(blank=True, null=True, verbose_name='index of output on target, if different from index'),
        ),
        migrations.AddField(
            model_name='sensor',
            name='target_index',
            field=models.IntegerField(blank=True, null=True, verbose_name='index of output on target, if different from index'),
        ),
        migrations.AlterField(
            model_name='outputrec',
            name='index',
            field=models.IntegerField(unique=True, verbose_name='index of output in interface'),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='index',
            field=models.IntegerField(unique=True, verbose_name='index of sensor in interface'),
        ),
    ]
