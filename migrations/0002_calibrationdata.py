# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2019-05-16 20:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_squashed_0013_auto_20180719_1738'),
    ]

    operations = [
        migrations.CreateModel(
            name='CalibrationData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sensor_reading', models.FloatField(verbose_name='value read from sensor')),
                ('real_value', models.FloatField(verbose_name='User measured value')),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name='time of measurement')),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Sensor')),
            ],
        ),
    ]
