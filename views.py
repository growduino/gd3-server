# -*- coding: utf-8 -*-
import datetime
import json
from time import sleep

from django.conf import settings
from django.core import mail
from django.core.mail.backends.smtp import EmailBackend
from django.http import HttpResponse, JsonResponse, Http404, HttpResponseRedirect
# from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django_basic_auth import logged_in_or_basicauth
from math import isinf, isnan

from core.forms import HtChangePass
from .models import Sensor, SensorData, Trigger, OutputRec, Alert, JsonStorage, OutputLog, RawDataStore, \
    save_config_to_file, new_config, save_config, SensorDataDaily, SensorDataHourly, CalibrationData
from .models import get_config
from .utils import crop_seconds, get_daymin, add_tzinfo, jsonize, set_htpass, rename_htuser, flags_to_security, \
    channelate, wifi_clear, wifi_start, eth_setup


# Create your views here.
def get_sensor_data(request, name, html=False):
    sensor = Sensor.objects.get(name__iexact=name)
    tto = crop_seconds()
    tfrom = tto - datetime.timedelta(hours=1)

    records = SensorData.objects.select_related('sensor').filter(time__gt=tfrom).filter(sensor=sensor)

    if records.count() == 0:
        raise Http404

    data = dict()
    for record in records:
        if record is not None:
            data[int((record.time - tfrom).seconds / 60) - 1] = record.value
    toret = jsonize(data, 60)

    resp = {"name": sensor.name, "min": toret}

    if html:
        return HttpResponse("<html><head></head><body>" + json.dumps(resp) + "</body></html>")

    return JsonResponse(resp)


def get_hourly_data(request, name, year, month, day, hour, html=False):
    sensor = Sensor.objects.get(name__iexact=name)

    time_from = datetime.datetime(year=int(year),
                                  month=int(month),
                                  day=int(day),
                                  hour=int(hour),
                                  )
    time_from = add_tzinfo(time_from)
    time_to = time_from + datetime.timedelta(hours=1)

    records = SensorData.objects.select_related('sensor').filter(sensor=sensor,
                                                                 time__gte=time_from,
                                                                 time__lt=time_to,
                                                                 )
    if records.count() == 0:
        raise Http404

    data = dict()
    for record in records:
        if record is not None:
            data[int(record.time.minute)] = record.value
    toret = jsonize(data, 60)
    if html:
        return HttpResponse("<html><head></head><body>" + ", ".join([str(a) for a in toret]) + "</body></html>")

    return JsonResponse({"name": sensor.name, "min": toret})


def get_hourly_average(request, name, year, month, day, html=False):  # returns averaged hours for given day
    sensor = Sensor.objects.get(name__iexact=name)

    records = SensorDataHourly.objects.select_related('sensor').filter(sensor=sensor, time__year=year,
                                                                       time__month=month, time__day=day)
    if records.count() == 0:
        raise Http404

    data = dict()
    for record in records:
        if record is not None:
            record.time = timezone.localtime(record.time)
            data[int(record.time.hour)] = record.value
    toret = jsonize(data, 24)
    if html:
        return HttpResponse("<html><head><title>get_daily_average()</title></head><body>" + ", ".join(
            [str(a) for a in toret]) + "</body></html>")

    return JsonResponse({"name": sensor.name, "h": toret})


def get_daily_average(request, name, year, month, html=False):
    sensor = Sensor.objects.get(name__iexact=name)

    records = SensorDataDaily.objects.select_related('sensor').filter(sensor=sensor, time__year=year, time__month=month)
    if records.count() == 0:
        raise Http404

    data = dict()
    for record in records:
        if record is not None:
            record.time = timezone.localtime(record.time)
            data[int(record.time.day)] = record.value
    toret = jsonize(data, 31, start=1)
    if html:
        return HttpResponse("<html><head><title>get_monthly_average()</title></head><body>" + ", ".join(
            [str(a) for a in toret]) + "</body></html>")

    return JsonResponse({"name": sensor.name, "day": toret})


@csrf_exempt
def get_trigger(request, index):
    """shows trigger as json
        {
            "t_since":-1,
            "t_until":0,
            "on_value":"<250",
            "off_value":">300",
            "sensor":1,
            "output":7,
            "active":1
        }"""
    if request.method == 'POST' and not request.body:
        trigger = get_object_or_404(Trigger, index=index)
        trigger.delete()
        return HttpResponse('<h1>Deleted</h1>')
    if request.body:
        return set_trigger(request, index)
    trigger = get_object_or_404(Trigger, index=index)

    # myjson = json.dumps(jsontg)
    return JsonResponse(trigger.as_dict())


# @logged_in_or_basicauth("Config")
def set_trigger(request, index):
    """creates or updates trigger
    not to be called directly"""
    # print(request.body.decode("utf-8"))
    jdata = json.loads(request.body.decode("utf-8"))
    # print(jdata)
    sensor = Sensor.objects.get(index=jdata['sensor'])

    output = OutputRec.objects.get(index=jdata['output'])

    trigger, created = Trigger.objects.update_or_create(index=index, defaults={
        't_since': jdata['t_since'],
        't_until': jdata['t_until'],
        'on_value': jdata['on_value'],
        'off_value': jdata['off_value'],
        'active': jdata['active'],
        'sensor': sensor,
        'output': output,
    })
    if created:
        resp = "Created"
    else:
        resp = "Updated"
    return HttpResponse('<h1>' + resp + '</h1>')


@csrf_exempt
def get_alert(request, index):
    """shows alert as json
    {
        "on_message":"Temp too high!",
        "off_message":"Temp back at normal",
        "trigger":1,
        "target":"+420777123456",
        active": 1
    }"""
    if request.method == 'POST' and not request.body:
        alert = get_object_or_404(Alert, index=index)
        alert.trigger.active = 0
        alert.trigger.save()
        alert.delete()
        return HttpResponse('<h1>Deleted</h1>')
    if request.body:
        return set_alert(request, index)
    alert = get_object_or_404(Alert, index=index)
    jsontg = dict()
    jsontg['on_message'] = alert.on_message
    jsontg['off_message'] = alert.off_message
    jsontg['trigger'] = alert.trigger.index
    jsontg['target'] = alert.target
    jsontg['active'] = alert.active

    # myjson = json.dumps(jsontg)
    return JsonResponse(jsontg)


def get_output_states(time=None):
    outputs = OutputRec.objects.filter(index__gte=0)
    result = ""
    for state in [o.get_state(time) for o in outputs]:
        if state:
            result += "1"
        else:
            result += "0"
    return result


def outputs(request, year=None, month=None, day=None, sequence=None):
    if year is None:  # posledni hodina
        start_time = timezone.now() - datetime.timedelta(hours=1)
        changes = OutputLog.objects.select_related('output').filter(output__index__gte=0).filter(time__gte=start_time)
    else:
        if str(sequence) != "0":
            raise Http404()
        changes = OutputLog.objects.select_related('output').filter(output__index__gte=0).filter(time__year=year,
                                                                                                 time__month=month,
                                                                                                 time__day=day)

    change_times = list(set([o.time for o in changes]))
    change_times.sort()

    states = {}
    for change_time in change_times:
        states[str(int(change_time.timestamp()))] = str(int(get_output_states(change_time)[::-1], 2))

    result = {"name": "outputs", "state": states}

    return JsonResponse(result)


def output_changes(request, year=None, month=None, day=None, sequence=None):
    if year is None:  # posledni hodina
        end_time = timezone.now()
        start_time = end_time - datetime.timedelta(hours=1)
        changes = OutputLog.objects.select_related('output').filter(output__index__gte=0).filter(time__gte=start_time)
    else:
        if str(sequence) != "0":
            raise Http404()
        changes = OutputLog.objects.filter(output__index__gte=0).filter(time__year=year, time__month=month,
                                                                        time__day=day)
        if changes:
            end_time = changes.latest().time
        else:
            end_time = timezone.datetime(year, month, day, tzinfo=timezone.now().tzinfo)

    states = {}

    for change in changes:
        change_time = str(int(change.time.timestamp()))
        if change_time not in states:
            states[change_time] = [{"output": change.output.name, "state": change.state}]
        else:
            states[change_time].append({"output": change.output.name, "state": change.state})

    end_state = get_output_states(end_time)

    result = {"name": "output_changes", "state": states, "end_state": str(int(end_state[::-1], 2)),
              "end_state_verbose": end_state}

    return JsonResponse(result)


@csrf_exempt
def output_states(request):
    result = get_output_states()
    jsontg = {"outputs": result}
    return JsonResponse(jsontg)


def set_alert(request, index):
    """creates or updates alert
    not to be called directly"""
    # print(request.body.decode("utf-8"))
    jdata = json.loads(request.body.decode("utf-8"))
    trigger, t_created = Trigger.objects.get_or_create(index=jdata['trigger'])

    alert, created = Alert.objects.update_or_create(index=index, defaults={
        'on_message': jdata['on_message'],
        'off_message': jdata['off_message'],
        'trigger': trigger,
        'target': jdata['target'],
        'active': jdata['active']
    })
    alert.trigger.active = 1
    alert.trigger.save()
    if created:
        resp = "Created"
    else:
        resp = "Updated"
    return HttpResponse('<h1>' + resp + '</h1>')


@csrf_exempt
@logged_in_or_basicauth("Logging")
def sensor_log(request, name=None, value=None):
    if name is not None:
        sensor = get_object_or_404(Sensor, name=name)
        if sensor.log(value) is not None:
            jsontg = {'result': 'ok'}
        else:
            jsontg = {'result': 'Invalid value'}
    else:
        if request.method == 'POST':
            data = json.loads(request.body.decode("utf-8"))
            if data:
                if 'rawvalue' in data:

                    sensor = Sensor.objects.get(name__iexact=data['sensor'])
                    raw_data, created = RawDataStore.objects.get_or_create(sensor=sensor)
                    raw_data.value = data['rawvalue']
                    raw_data.measured = True
                    raw_data.save()
                else:
                    for record in data:
                        sensor = Sensor.objects.get(name=record['name'])
                        sensor.log(record['value'])
                jsontg = {'result': 'ok'}
            else:
                jsontg = {'result': 'noop'}
        else:
            jsontg = {'result': 'Invalid data'}

    return JsonResponse(jsontg)


def status(request):
    """
    {
        "free_ram":1025,
        "sensors":6,
        "outputs":8,
        "sensor_list":{
            "0":"Humidity",
            "1":"Temp1",
            "2":"Light",
            "3":"Usnd",
            "4":"Temp2",
            "5":"Temp3"
        },
        "triggers":10,
        "triggers_log_size":25,
        "uptime":"3381",
        "daymin":234
    }"""
    sensors = Sensor.objects.filter(index__gte=0)
    sensor_list = {}
    for s in sensors:
        sensor_list[str(s.index)] = s.name

    toret = {'sensors': sensors.count(),
             'outputs': 12,
             'triggers': 100,
             'daymin': get_daymin(),
             'sensor_list': sensor_list,
             'time_zone': getattr(settings, 'TIME_ZONE', 'Error'),
             }
    return JsonResponse(toret)


@csrf_exempt
# @logged_in_or_basicauth("Config")
def json_store(request, filename='config'):
    if request.method == 'POST':
        if not request.body:
            # if filename == 'client':
            config = new_config(filename)
            save_config(config, filename)
            if filename == 'config':
                save_config_to_file(get_config(filename=filename), filename)
            announce_config_change(filename)
            return HttpResponse('<h1>Deleted</h1>')
        else:
            save_config(json.loads(request.body.decode("utf-8")), filename=filename)
            # request_config = json.loads()
            # db_config = get_config(filename=filename)
            # if request_config != db_config:
            #     config_db = JsonStorage(value=request.body.decode("utf-8"), filename=filename)
            #     config_db.save()
            #     if filename == 'config':
            #         save_config_to_file(request_config, filename)
            announce_config_change(filename)

    return JsonResponse(get_config(filename=filename))


def announce_config_change(filename):
    if filename == 'config':
        config = get_config()

        if 'wifi_ssid' in config and config['wifi_ssid']:
            ssid = config['wifi_ssid']
            password = None
            if 'wifi_pwd' in config and config['wifi_ssid']:
                password = config['wifi_pwd']

            wifi_start(ssid, password)
        else:
            wifi_clear()
        sleep(10)
        eth_setup()
    import pika
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange='sensors',
                             exchange_type='direct')
    channel.basic_publish(exchange='sensors',
                          routing_key='gd3',
                          body=filename
                          )


@csrf_exempt
def json_store_partial(request, filename):
    if request.method == 'POST':
        if not request.body:
            return HttpResponse('<h1>Failure</h1>')
        request_config = json.loads(request.body.decode("utf-8"))
        db_config = get_config(filename=filename)

        for field in request_config:
            if request_config[field] in (None, ""):
                db_config.pop(field, None)
            else:
                db_config[field] = request_config[field]
        config_db = JsonStorage(value=json.dumps(db_config), filename=filename)
        config_db.save()
        if filename == 'config':
            save_config_to_file(db_config, filename)

        announce_config_change(filename)
        return HttpResponse('<h1>Success</h1>')
    return HttpResponse('<h1>Error</h1>')


@csrf_exempt
@logged_in_or_basicauth("Logging")
def sensor_calibrate(request):
    jsontg = {'result': 'ok'}
    return JsonResponse(jsontg)


import NetworkManager


def wifi_list(request):
    wifis = {}
    for dev in NetworkManager.NetworkManager.GetDevices():
        if dev.DeviceType != NetworkManager.NM_DEVICE_TYPE_WIFI:
            continue
        for ap in dev.GetAccessPoints():
            security = flags_to_security(ap.Flags, ap.WpaFlags, ap.RsnFlags)
            wifi = {
                "ssid": ap.Ssid,
                "frequency": ap.Frequency,
                "encrypted": security != "",
                "security": security,
                "quality": "{str}%".format(str=ap.Strength),
                "channel": channelate(ap.Frequency)
            }
            str = ap.Strength
            while str in wifis:
                str += 1
            wifis[str] = wifi
    sorted_wifi = []
    for key in sorted(wifis.keys()):
        sorted_wifi.append(wifis[key])
    sorted_wifi.reverse()
    return JsonResponse({"networks": sorted_wifi})

    # logged_in_or_basicauth("Config")


def wifi_active(request):
    ssid = None
    for conn in NetworkManager.NetworkManager.ActiveConnections:
        settings = conn.Connection.GetSettings()
        if "802-11-wireless" in settings:
            ssid = settings['802-11-wireless']['ssid']
    return JsonResponse({"ssid": ssid})


def calibrate(request, index):
    sensor = get_object_or_404(Sensor, index=index)
    sensor.raw_data.all().delete()

    line_to_send = "get raw {name}".format(name=sensor.name.lower())

    import pika

    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange='sensors',
                             exchange_type='direct')

    channel.basic_publish(exchange='sensors',
                          routing_key='gd3',
                          body=line_to_send
                          )

    start_time = datetime.datetime.now()
    duration = datetime.timedelta(seconds=20)
    stop_time = start_time + duration

    while sensor.raw_data.filter(measured=True).count() == 0:
        if datetime.datetime.now() > stop_time:
            return HttpResponse(status=504)
        sleep(1)

    raw_data = sensor.raw_data.filter(measured=True)[0]
    raw_value = raw_data.value
    if isinf(raw_value) or isnan(raw_value):
        raw_value = -999
    toret = {"raw_value": raw_value}
    sensor.raw_data.all().delete()
    return JsonResponse(toret)


def password_change(request):
    if request.method == 'POST':
        form = HtChangePass(request.POST)
        if form.is_valid():
            done = False
            username = form.cleaned_data['username']
            new_password = form.cleaned_data['new_password']
            old_password = form.cleaned_data['old_password']
            rename_to = form.cleaned_data['rename_to']
            if rename_to:
                rename_htuser(username, rename_to, old_password)
                username = rename_to
                done = True
            if new_password:
                set_htpass(username, new_password, old_password)
                done = True
            if done:
                return HttpResponseRedirect('/')
    else:
        form = HtChangePass()

    return render(request, 'core/password_change.html', {'form': form})


def send_test_mail(request):
    if 'to' in request.GET and "@" in request.GET['to']:
        target = (request.GET['to'],)
        host = request.GET.get('smtp')
        port = request.GET.get('smtp_port')
        username = request.GET.get('smtp_user')
        password = request.GET.get('smtp_pwd')
        smtp_ssl = request.GET.get('smtp_ssl')
        use_tls = smtp_ssl == 'tls'
        use_ssl = smtp_ssl == 'ssl'
        mail_from = request.GET.get('mail_from')

    else:
        return JsonResponse({'success': False, 'code': "{resp}".format(resp="missing sender")})

    subject = '[grdw] test mail'
    body = 'Hello, this is just testing mail.'

    debug_smtp = {
        'host': host,
        'port': port,
        'username': username,
        'password': password,
        'use_ssl': use_ssl,
        'use_tls': use_tls,
        'from_email': mail_from,
        'to': target,
    }

    try:
        backend = EmailBackend(
            host=host,
            port=port,
            username=username,
            password=password,
            use_ssl=use_ssl,
            use_tls=use_tls,
            timeout=5,
        )

        resp = mail.EmailMessage(
            subject=subject,
            body=body,
            from_email=mail_from,
            to=target,
            connection=backend,
        ).send()
    except Exception as err:
        return JsonResponse({'success': False, 'code': "{resp}".format(resp=err), 'settings': debug_smtp})

    return JsonResponse({'success': True, 'settings': debug_smtp})


@csrf_exempt
def calib_new(request, sensor):
    calib = CalibrationData()
    sensor = get_object_or_404(Sensor, id=sensor)

    if request.method == "POST":
        calib.sensor = Sensor.objects.get(index=sensor)
        calib.sensor_reading = request.POST['sensor_reading']
        calib.real_value = request.POST['real_value']
        calib.save()
        sensor.update_calibration()
        return JsonResponse({'success': True, 'data': calib.as_dict()})
    return JsonResponse({'success': False})


def calib_list(request, sensor):
    sensor = get_object_or_404(Sensor, index=sensor)
    json = {calib.id: calib.as_dict() for calib in sensor.CalibrationData.all()}
    return JsonResponse(json)


def calib_get(request, sensor, id):
    calib = get_object_or_404(CalibrationData, id=id, sensor=Sensor.objects.get(index=sensor))
    return JsonResponse(calib.as_dict())


def calib_delete(request, sensor, id):
    calib = get_object_or_404(CalibrationData, id=id, sensor=Sensor.objects.get(index=sensor))
    calib.delete()
    sensor = Sensor.objects.get(index=sensor)
    sensor.update_calibration()
    return JsonResponse({'success': True })


@csrf_exempt
def calib_set(request, sensor, id):
    calib = get_object_or_404(CalibrationData, id=id, sensor=Sensor.objects.get(index=sensor))
    if request.method == "POST":
        calib.sensor_reading = request.POST['sensor_reading']
        calib.real_value = request.POST['real_value']
        calib.save()
        sensor.update_calibration()
        return JsonResponse({'success': True, 'data': calib.as_dict()})
    return JsonResponse({'success': False})
