from django import forms

from .utils import check_htpass


class HtChangePass(forms.Form):
    username = forms.CharField(label="Username", max_length=30)
    old_password = forms.CharField(label="Old password", max_length=100, widget=forms.PasswordInput)
    new_password = forms.CharField(label="New password", max_length=100, widget=forms.PasswordInput, required=False)
    new_password_2 = forms.CharField(label="Retype password", max_length=100, widget=forms.PasswordInput,
                                     required=False)
    rename_to = forms.CharField(label="New username", max_length=30, help_text="only if you want to change it",
                                required=False)

    def clean(self):
        # Use the parent's handling of required fields, etc.
        cleaned_data = super().clean()
        username = cleaned_data['username']
        old_password = cleaned_data['old_password']
        if username and old_password:
            if not check_htpass(username, old_password):
                raise forms.ValidationError(
                    'Invalid credentials',
                    code='invalid',
                )
        new_password = cleaned_data['new_password']
        new_password_2 = cleaned_data['new_password_2']
        if new_password or new_password_2:
            if new_password != new_password_2:
                raise forms.ValidationError(
                    'New passwords differ',
                    code='invalid'
                )
